package com.example.nikolai.engadgetnews.Profile;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.nikolai.engadgetnews.AuthAndSignUp.Auth;
import com.example.nikolai.engadgetnews.R;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

import static android.app.Activity.RESULT_OK;
import static com.theartofdev.edmodo.cropper.CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE;
import static com.theartofdev.edmodo.cropper.CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE;
import static com.theartofdev.edmodo.cropper.CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE;

public class Profile extends Fragment {

    private final String TAG = "log_tag";

    private CircleImageView imageView;
    private Uri cropImageUri;
    private Uri uri;

    private TextView tvName;
    private TextView tvEmail;
    private TextView tvPhone;

    private String userName;

    private Realm realm;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile, container, false);
        setHasOptionsMenu(true);

        imageView = (CircleImageView) view.findViewById(R.id.imageViewAvatar);

        tvName = (TextView) view.findViewById(R.id.tvFirstAndLastName);
        tvEmail = (TextView) view.findViewById(R.id.tvEmail);
        tvPhone = (TextView) view.findViewById(R.id.tvPhone);

        writeProfile();

        return view;
    }

    private void writeProfile() {
        Realm.init(getActivity());
        RealmConfiguration realmConfiguration = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        realm = Realm.getInstance(realmConfiguration);

        userName = this.getArguments().getString("userName");

        Log.d(TAG, "writeProfile: userName = " + userName);

        RealmResults<User> user = realm.where(User.class).findAll();

        User userData = user.where()
                .equalTo("userName", userName)
                .findFirst();

        Picasso.with(getContext())
                .load(userData.getImagePath())
                .placeholder(android.R.drawable.ic_menu_report_image)
                .error(android.R.drawable.ic_menu_report_image)
                .into(imageView);

        tvName.setText(userData.getFirstName() + " " + userData.getLastName());
        tvEmail.setText(userData.getEmail());
        tvPhone.setText(userData.getPhone());
    }

    public static Profile newInstance(final String userName) {
        final Profile profile = new Profile();
        final Bundle bundle = new Bundle();
        bundle.putString("userName", userName);
        profile.setArguments(bundle);
        return profile;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_profile, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @SuppressLint("NewApi")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.changeAvatar:
                if (CropImage.isExplicitCameraPermissionRequired(getContext()))
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
                else
                    CropImage.startPickImageActivity(getActivity());
                return true;
            case R.id.changeProfile:
                intent = new Intent(getContext(), EditProfile.class);
                intent.putExtra("userName", userName);
                startActivity(intent);
                return true;
            case R.id.exitProfile:
                intent = new Intent(getContext(), Auth.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult: requestCode = " + requestCode);

        if (requestCode == PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getContext(), data);
            if (CropImage.isReadExternalStoragePermissionsRequired(getActivity(), imageUri)) {
                cropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            } else {
                CropImage.activity(imageUri)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setMultiTouchEnabled(true)
                        .start(getContext(), this);
            }
        }

        if (requestCode == CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                imageView.setImageURI(result.getUri());
                uri = result.getUri();

                RealmResults<User> user = realm.where(User.class).findAll();

                User userData = user.where()
                        .equalTo("userName", userName)
                        .findFirst();

                realm.beginTransaction();
                userData.setImagePath(uri.getScheme() + ":" + uri.getSchemeSpecificPart());
                realm.commitTransaction();
            } else if (resultCode == CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE)
                Snackbar.make(imageView, "Error", Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onActivityResult: requestCode = " + requestCode);

        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (cropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                CropImage.activity(cropImageUri)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setMultiTouchEnabled(true)
                        .start(getContext(), this);
            } else
                Snackbar.make(imageView, "Error", Snackbar.LENGTH_LONG).show();
        }
    }
}
