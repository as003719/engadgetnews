package com.example.nikolai.engadgetnews.Profile;

import com.example.nikolai.engadgetnews.MyFeed.MyArticle;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class User extends RealmObject {

    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    @PrimaryKey
    private String userName;
    private String password;
    private String imagePath;
    private RealmList<MyArticle> myArticles;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public RealmList<MyArticle> getMyArticles() {
        return myArticles;
    }

    public void setMyArticles(RealmList<MyArticle> myArticles) {
        this.myArticles = myArticles;
    }
}
