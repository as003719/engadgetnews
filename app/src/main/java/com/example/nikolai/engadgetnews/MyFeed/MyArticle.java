package com.example.nikolai.engadgetnews.MyFeed;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class MyArticle extends RealmObject {

    @PrimaryKey
    private String title;
    private String description;
    private String imageArticle;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageArticle() {
        return imageArticle;
    }

    public void setImageArticle(String imageArticle) {
        this.imageArticle = imageArticle;
    }
}
