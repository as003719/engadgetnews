package com.example.nikolai.engadgetnews.Feed;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nikolai.engadgetnews.R;
import com.squareup.picasso.Picasso;

import io.realm.RealmResults;

public class RVAdapterArticle extends RecyclerView.Adapter<RVAdapterArticle.ViewHolder> {

    private RealmResults<Article> results;

    public RVAdapterArticle(RealmResults<Article> results) {
        this.results = results;
    }

    @Override
    public RVAdapterArticle.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.tvTitle.setText(results.get(position).getTitle());
        holder.tvDescription.setText(results.get(position).getDescription());
        holder.tvPublished.setText(results.get(position).getPublishedAt());
        Picasso.with(holder.cardView.getContext())
                .load(results.get(position).getUrlToImage())
                .error(android.R.drawable.ic_menu_report_image)
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        ImageView imageView;
        TextView tvTitle;
        TextView tvDescription;
        TextView tvPublished;

        ViewHolder(View view) {
            super(view);
            cardView = (CardView) view.findViewById(R.id.card_view);
            imageView = (ImageView) view.findViewById(R.id.imageView);
            tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            tvDescription = (TextView) view.findViewById(R.id.tvDescription);
            tvPublished = (TextView) view.findViewById(R.id.tvPublished);
        }
    }
}
