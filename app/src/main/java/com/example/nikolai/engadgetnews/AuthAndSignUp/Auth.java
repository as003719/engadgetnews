package com.example.nikolai.engadgetnews.AuthAndSignUp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.nikolai.engadgetnews.Profile.User;
import com.example.nikolai.engadgetnews.R;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class Auth extends AppCompatActivity {

    private final String TAG = "log_tag";

    private EditText etUser;
    private EditText etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.auth);

        etUser = (EditText) findViewById(R.id.editTextUserName);
        etPassword = (EditText) findViewById(R.id.editTextPassword);
        Button btnLogin = (Button) findViewById(R.id.btnLogin);
        Button btnSignUp = (Button) findViewById(R.id.btnSignUp);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                authorization(view);
            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SignUp.class);
                startActivity(intent);
            }
        });
    }

    private void authorization(View view) {
        Realm.init(getApplicationContext());
        RealmConfiguration realmConfiguration = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm realm = Realm.getInstance(realmConfiguration);

        RealmResults<User> user = realm.where(User.class).findAll();

        if (user.size() == 0) {
            Snackbar.make(view, "Please, create an account", Snackbar.LENGTH_LONG).show();
        } else {
            for (int i = 0; i < user.size(); i++) {
                if (user.get(i).getUserName().equals(etUser.getText().toString()) &&
                        user.get(i).getPassword().equals(etPassword.getText().toString())) {
                    Log.d(TAG, "authorization: user size = " + user.size());
                    Log.d(TAG, "authorization: userName = " + user.get(i).getUserName());
                    Log.d(TAG, "authorization: password = " + user.get(i).getPassword());
                    Intent intent = new Intent(getApplicationContext(), Confirmation.class);
                    intent.putExtra("userName", user.get(i).getUserName());
                    startActivity(intent);
                } else
                    Snackbar.make(view, "Check your entries", Snackbar.LENGTH_LONG).show();
            }
        }
    }
}
