package com.example.nikolai.engadgetnews.Feed;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.nikolai.engadgetnews.R;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Feed extends Fragment {

    private final String TAG = "log_tag";

    private Realm realm;
    private RealmResults<Article> art;

    private RecyclerView recyclerView;
    private RVAdapterArticle adapter;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.feed_recycler_view, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ResponseColumn();
            }
        });

        ResponseColumn();

        return view;
    }

    private void ResponseColumn() {
        Gson gson = new GsonBuilder().setExclusionStrategies(new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes f) {
                return f.getDeclaredClass().equals(RealmObject.class);
            }

            @Override
            public boolean shouldSkipClass(Class<?> clazz) {
                return false;
            }
        }).create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://newsapi.org/v1/articles/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        EngadgetAPI engadgetAPI = retrofit.create(EngadgetAPI.class);

        Call<EngadgetArticle> call = engadgetAPI.getArticle();
        call.enqueue(new Callback<EngadgetArticle>() {
            @Override
            public void onResponse(Call<EngadgetArticle> call, Response<EngadgetArticle> response) {

                Realm.init(getActivity());
                RealmConfiguration config = new RealmConfiguration
                        .Builder()
                        .deleteRealmIfMigrationNeeded()
                        .build();
                Realm.setDefaultConfiguration(config);

                realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                realm.copyToRealmOrUpdate(response.body().getArticles());
                realm.commitTransaction();

                art = realm.where(Article.class).findAllSorted("publishedAt", Sort.DESCENDING);
                art.addChangeListener(new RealmChangeListener<RealmResults<Article>>() {
                    @Override
                    public void onChange(RealmResults<Article> element) {
                        adapter.notifyDataSetChanged();
                    }
                });

                adapter = new RVAdapterArticle(art);
                recyclerView.setAdapter(adapter);

                Log.d(TAG, "onResponse: realm first item = " + art.get(0).getTitle());
                Log.d(TAG, "onResponse: realm size = " + art.size());
                Log.d(TAG, "onResponse: " + response.message());
            }

            @Override
            public void onFailure(Call<EngadgetArticle> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
