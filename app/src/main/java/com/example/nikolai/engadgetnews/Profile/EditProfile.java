package com.example.nikolai.engadgetnews.Profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.nikolai.engadgetnews.MainActivity;
import com.example.nikolai.engadgetnews.R;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class EditProfile extends AppCompatActivity {

    private final String TAG = "log_tag";

    private EditText editFirstName;
    private EditText editLastName;
    private EditText editEmail;
    private EditText editPhone;

    private Button btnSaveEdit;

    private String userName;

    private Realm realm;

    private static final String PROFILE_ID = "Profile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile);

        editFirstName = (EditText) findViewById(R.id.etFirstNameEdit);
        editLastName = (EditText) findViewById(R.id.etLastNameEdit);
        editEmail = (EditText) findViewById(R.id.etEmailEdit);
        editPhone = (EditText) findViewById(R.id.etPhoneEdit);

        btnSaveEdit = (Button) findViewById(R.id.btnSaveEdit);
        btnSaveEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                RealmResults<User> user = realm.where(User.class).findAll();
                User userData = user.where()
                        .equalTo("userName", userName)
                        .findFirst();

                realm.beginTransaction();
                userData.setFirstName(editFirstName.getText().toString());
                userData.setLastName(editLastName.getText().toString());
                userData.setEmail(editEmail.getText().toString());
                userData.setPhone(editPhone.getText().toString());
                userData = realm.copyToRealmOrUpdate(userData);
                realm.commitTransaction();

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("userName", userName);
                intent.putExtra("ID", PROFILE_ID);
                startActivity(intent);
            }
        });

        editProfile();
    }

    private void editProfile() {
        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        realm = Realm.getInstance(realmConfiguration);

        userName = getIntent().getStringExtra("userName");

        Log.d(TAG, "editProfile: userName = " + userName);


        RealmResults<User> user = realm.where(User.class).findAll();
        User userData = user.where()
                .equalTo("userName", userName)
                .findFirst();

        editFirstName.setText(userData.getFirstName());
        editLastName.setText(userData.getLastName());
        editEmail.setText(userData.getEmail());
        editPhone.setText(userData.getPhone());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
