package com.example.nikolai.engadgetnews.AuthAndSignUp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.nikolai.engadgetnews.MainActivity;
import com.example.nikolai.engadgetnews.R;

public class Confirmation extends AppCompatActivity {

    private EditText editText;
    private Button button;

    private static final String code = "1111";

    private static final String FEED_ID = "Feed";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.confirmation);

        editText = (EditText) findViewById(R.id.etCode);
        button = (Button) findViewById(R.id.btnConf);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editText.getText().toString().equals(code)) {
                    String str = getIntent().getStringExtra("userName");

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.putExtra("userName", str);
                    intent.putExtra("ID", FEED_ID);
                    startActivity(intent);
                } else {
                    Snackbar.make(view, "Incorrect code!", Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }
}
