package com.example.nikolai.engadgetnews.MyFeed;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nikolai.engadgetnews.R;
import com.squareup.picasso.Picasso;

import io.realm.RealmList;

public class RVAdapterMyArticle extends RecyclerView.Adapter<RVAdapterMyArticle.ViewHolder> {

    private RealmList<MyArticle> realmResults;

    public RVAdapterMyArticle(RealmList<MyArticle> realmResults) {
        this.realmResults = realmResults;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.tvTitle.setText(realmResults.get(position).getTitle());
        holder.tvDescription.setText(realmResults.get(position).getDescription());
        Picasso.with(holder.cardView.getContext())
                .load(realmResults.get(position).getImageArticle())
                .error(android.R.drawable.ic_menu_report_image)
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return realmResults.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        ImageView imageView;
        TextView tvTitle;
        TextView tvDescription;

        ViewHolder(View view) {
            super(view);
            cardView = (CardView) view.findViewById(R.id.card_view);
            imageView = (ImageView) view.findViewById(R.id.imageView);
            tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            tvDescription = (TextView) view.findViewById(R.id.tvDescription);
        }
    }
}
