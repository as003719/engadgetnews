package com.example.nikolai.engadgetnews.MyFeed;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.nikolai.engadgetnews.Profile.User;
import com.example.nikolai.engadgetnews.R;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmResults;

public class MyFeed extends Fragment {

    private final String TAG = "log_tag";

    private Realm realm;

    private FloatingActionButton fab;

    private RecyclerView recyclerView;
    private RVAdapterMyArticle adapter;

    private String userName;


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_feed, container, false);

        fab = (FloatingActionButton) view.findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AddArticle.class);
                intent.putExtra("userName", userName);
                startActivity(intent);
            }
        });

        recyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        writeFeed();

        return view;
    }

    public static MyFeed newInstance(final String userName) {
        final MyFeed myFeed = new MyFeed();
        final Bundle bundle = new Bundle();
        bundle.putString("userName", userName);
        myFeed.setArguments(bundle);
        return myFeed;
    }

    private void writeFeed() {
        Realm.init(getActivity());
        RealmConfiguration realmConfiguration = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        realm = Realm.getInstance(realmConfiguration);

        userName = this.getArguments().getString("userName");

        RealmResults<User> user = realm.where(User.class).findAll();

        User userData = user.where()
                .equalTo("userName", userName)
                .findFirst();

        RealmList<MyArticle> myArticles = userData.getMyArticles();

        Log.d(TAG, "onCreateView: " + myArticles.size());
        Log.d(TAG, "onCreateView: userName myFeed = " + userName);

        adapter = new RVAdapterMyArticle(myArticles);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
