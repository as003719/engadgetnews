package com.example.nikolai.engadgetnews;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import com.example.nikolai.engadgetnews.Feed.Feed;
import com.example.nikolai.engadgetnews.MyFeed.MyFeed;
import com.example.nikolai.engadgetnews.Profile.Profile;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private final String TAG = "log_tag";

    private Fragment fragment;
    private FragmentManager fragmentManager;

    private String userName;
    private String ID;

    private static final String FEED_ID = "Feed";
    private static final String MY_FEED_ID = "MyFeed";
    private static final String PROFILE_ID = "Profile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);

        userName = getIntent().getStringExtra("userName");
        ID = getIntent().getStringExtra("ID");

        if (fragment == null) {
            switch (ID) {
                case FEED_ID:
                    fragment = new Feed();
                    bottomNavigationView.getMenu().findItem(R.id.action_feed).setChecked(true);
                    break;
                case MY_FEED_ID:
                    fragment = MyFeed.newInstance(userName);
                    bottomNavigationView.getMenu().findItem(R.id.action_my_feed).setChecked(true);
                    break;
                case PROFILE_ID:
                    fragment = Profile.newInstance(userName);
                    bottomNavigationView.getMenu().findItem(R.id.action_profile).setChecked(true);
                    break;
            }

            fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .add(R.id.main_container, fragment)
                    .addToBackStack(null)
                    .commit();
        }

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_feed:
                        fragment = new Feed();
                        break;
                    case R.id.action_my_feed:
                        fragment = MyFeed.newInstance(userName);
                        break;
                    case R.id.action_profile:
                        fragment = Profile.newInstance(userName);
                        break;
                }

                fragmentManager.beginTransaction()
                        .replace(R.id.main_container, fragment)
                        .addToBackStack(null)
                        .commit();
                return true;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: requestCode = " + requestCode);

        List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
        if (fragmentList == null || fragmentList.size() == 0)
            return;

        for (Fragment fragment1 : fragmentList) {
            if (fragment1 != null && !fragment1.isDetached() && !fragment1.isRemoving())
                fragment1.onActivityResult(requestCode, resultCode, data);
        }
    }
}
