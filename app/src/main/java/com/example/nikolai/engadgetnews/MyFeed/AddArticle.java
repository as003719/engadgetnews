package com.example.nikolai.engadgetnews.MyFeed;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.nikolai.engadgetnews.MainActivity;
import com.example.nikolai.engadgetnews.Profile.User;
import com.example.nikolai.engadgetnews.R;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;

import static com.theartofdev.edmodo.cropper.CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE;
import static com.theartofdev.edmodo.cropper.CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE;
import static com.theartofdev.edmodo.cropper.CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE;

public class AddArticle extends AppCompatActivity {

    private final String TAG = "log_tag";

    private EditText title;
    private EditText description;

    private ImageView imageView;

    private String userName;

    private Uri cropImageUri;
    private Uri uri;

    private static final String MY_FEED_ID = "MyFeed";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_article);

        title = (EditText) findViewById(R.id.editTextTitle);
        description = (EditText) findViewById(R.id.editTextDescription);

        imageView = (ImageView) findViewById(R.id.imageViewArticle);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            @SuppressLint("NewApi")
            public void onClick(View view) {
                if (CropImage.isExplicitCameraPermissionRequired(AddArticle.this))
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
                else
                    CropImage.startPickImageActivity(AddArticle.this);
            }
        });

        Button btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (title.getText().toString().isEmpty() && description.getText().toString().isEmpty())
                    Snackbar.make(view, "Enter data", Snackbar.LENGTH_LONG).show();
                else {
                    saveArticle();

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.putExtra("userName", userName);
                    intent.putExtra("ID", MY_FEED_ID);
                    startActivity(intent);
                }
            }
        });
    }

    private void saveArticle() {
        Realm.init(getApplicationContext());
        RealmConfiguration realmConfiguration = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm realm = Realm.getInstance(realmConfiguration);

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                try {
                    userName = getIntent().getStringExtra("userName");

                    User userData = realm.where(User.class)
                            .equalTo("userName", userName)
                            .findFirst();
                    RealmList<MyArticle> articleList = userData.getMyArticles();
                    MyArticle article = new MyArticle();
                    article.setTitle(title.getText().toString());
                    article.setDescription(description.getText().toString());
                    article.setImageArticle(uri.getScheme()+ ":" + uri.getSchemeSpecificPart());
                    article = realm.copyToRealm(article);
                    articleList.add(article);

                    Log.d(TAG, "execute: getArt = " + userData.getMyArticles().size());
                } catch (Exception e) {
                    e.printStackTrace();
                    realm.cancelTransaction();
                }
            }
        });
    }

    @Override
    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getApplicationContext(), data);

            if (CropImage.isReadExternalStoragePermissionsRequired(getApplicationContext(), imageUri)) {
                cropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            } else {
                CropImage.activity(imageUri)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setMultiTouchEnabled(true)
                        .start(this);
            }
        }

        if (requestCode == CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                imageView.setImageURI(result.getUri());
                uri = result.getUri();
            } else if (resultCode == CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE)
                Snackbar.make(imageView, "Error", Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (cropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                CropImage.activity(cropImageUri)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setMultiTouchEnabled(true)
                        .start(this);
            } else
                Snackbar.make(imageView, "Error", Snackbar.LENGTH_LONG).show();
        }
    }
}
