package com.example.nikolai.engadgetnews.Feed;

import retrofit2.Call;
import retrofit2.http.GET;

interface EngadgetAPI {
    @GET("?source=engadget&sortBy=latest&apiKey=eca9819850b64dafbd7513908f89bf16")
    Call<EngadgetArticle> getArticle();
}
