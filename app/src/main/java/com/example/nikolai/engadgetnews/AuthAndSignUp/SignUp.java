package com.example.nikolai.engadgetnews.AuthAndSignUp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import com.example.nikolai.engadgetnews.Profile.User;
import com.example.nikolai.engadgetnews.R;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmResults;

import static com.theartofdev.edmodo.cropper.CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE;
import static com.theartofdev.edmodo.cropper.CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE;
import static com.theartofdev.edmodo.cropper.CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE;

public class SignUp extends AppCompatActivity {

    private final String TAG = "log_tag";

    private String userName;

    private RadioButton radioButtonEmail;
    private RadioButton radioButtonPhone;

    private EditText password;
    private EditText passwordAgain;
    private EditText firstName;
    private EditText lastName;
    private EditText email;
    private EditText phone;

    private CircleImageView imageView;
    private Uri cropImageUri;
    private Uri uri;

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up);

        radioButtonEmail = (RadioButton) findViewById(R.id.radioButtonEmail);
        radioButtonPhone = (RadioButton) findViewById(R.id.radioButtonPhone);

        password = (EditText) findViewById(R.id.etPassword);
        passwordAgain = (EditText) findViewById(R.id.etPasswordAgain);
        firstName = (EditText) findViewById(R.id.etFirstName);
        lastName = (EditText) findViewById(R.id.etLastName);
        email = (EditText) findViewById(R.id.etEmail);
        phone = (EditText) findViewById(R.id.etPhone);

        uri = Uri.parse("@android:drawable/ic_menu_report_image");

        Log.d(TAG, "onCreate: uri " + uri.getScheme());
        Log.d(TAG, "onCreate: uri " + uri.getSchemeSpecificPart());

        imageView = (CircleImageView) findViewById(R.id.signAvatar);
        imageView.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View view) {
                if (CropImage.isExplicitCameraPermissionRequired(SignUp.this))
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
                else
                    CropImage.startPickImageActivity(SignUp.this);
            }
        });

        Realm.init(getApplicationContext());
        RealmConfiguration realmConfiguration = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        realm = Realm.getInstance(realmConfiguration);

        Button signUp = (Button) findViewById(R.id.buttonSignUp);
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (radioButtonEmail.isChecked())
                    userName = email.getText().toString();
                else if (radioButtonPhone.isChecked())
                    userName = phone.getText().toString();

                RealmResults<User> user = realm.where(User.class).findAll();
                User userData = user.where()
                        .equalTo("userName", userName)
                        .findFirst();

                if (userData != null)
                    Snackbar.make(view, "The login is already in use", Snackbar.LENGTH_LONG).show();
                else if (email.getText().toString().isEmpty() ||
                        phone.getText().toString().isEmpty() ||
                        firstName.getText().toString().isEmpty() ||
                        lastName.getText().toString().isEmpty() ||
                        password.getText().toString().isEmpty())
                    Snackbar.make(view, "Enter data", Snackbar.LENGTH_LONG).show();
                else if (password.getText().toString().equals(passwordAgain.getText().toString())) {
                    addDataProfile();
                    Log.d(TAG, "onClick: username get = " + userName);
                    Intent intent = new Intent(getApplicationContext(), Confirmation.class);
                    intent.putExtra("userName", userName);
                    startActivity(intent);
                } else
                    Snackbar.make(view, "Check your password", Snackbar.LENGTH_LONG).show();
                
            }
        });
    }

    private void addDataProfile() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                try {
                    RealmList<User> userList = new RealmList<User>();
                    User user = realm.createObject(User.class, userName);
                    user.setPassword(password.getText().toString());
                    user.setFirstName(firstName.getText().toString());
                    user.setLastName(lastName.getText().toString());
                    user.setEmail(email.getText().toString());
                    user.setPhone(phone.getText().toString());
                    user.setImagePath(uri.getScheme() + ":" + uri.getSchemeSpecificPart());
                    user = realm.copyToRealm(user);
                    userList.add(user);

                    Log.d(TAG, "execute: realm size = " + realm.where(User.class).findAll().size());
                } catch (Exception e) {
                    e.printStackTrace();
                    realm.cancelTransaction();
                }
            }
        });
    }

    @Override
    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getApplicationContext(), data);

            if (CropImage.isReadExternalStoragePermissionsRequired(getApplicationContext(), imageUri)) {
                cropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            } else {
                CropImage.activity(imageUri)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setMultiTouchEnabled(true)
                        .start(this);
            }
        }

        if (requestCode == CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                imageView.setImageURI(result.getUri());
                uri = result.getUri();
            } else if (resultCode == CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE)
                Snackbar.make(imageView, "Error", Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (cropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                CropImage.activity(cropImageUri)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setMultiTouchEnabled(true)
                        .start(this);
            } else
                Snackbar.make(imageView, "Error", Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
